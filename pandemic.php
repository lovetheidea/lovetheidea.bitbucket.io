<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple Markers</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>

      function initMap() {
        var myLatLng = {lat: -25.363, lng: 131.044};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
    </script>


<?php


$SCOTLAND__MAP = array(
"Mon" => array(0,6),
"Tue" => array(1,8),
"Wed" => array(3,7),
"Thu" => array(2,9),
"Fri" => array(4,5),
"Sat" => array(1,8),
"Sun" => array(6,9),
);
$SCOTLAND_POSTCODES = array(
   "AB-Aberdeen",
   "DD-Dundee",
   "KW-Kirkwall",
   "DG-Dumfries",
   "KY-kirkcaldy",
   "EH-Edinburgh",
   "ML-Motherwell",
   "FK-Falkirk",
   "PA-Paisley",
   "G-Glasgow",
   "PH-Perth",
   "TD-Galashiels",
   "IV-Inverness"
);
echo("<b>Open SCOTLAND_POSTCODES</b>" . "<br><ul>");
$day = date('D');
foreach($SCOTLAND__MAP[$day] as $d) {
    printf("<li>".$SCOTLAND_POSTCODES[$d] . "</li>");
}
echo "</ul>";


$NORTH_EAST_MAP = array(
"Mon" => array(0,6),
"Tue" => array(1,8),
"Wed" => array(3,7),
"Thu" => array(2,9),
"Fri" => array(4,5),
"Sat" => array(1,8),
"Sun" => array(6,9),
);
$NORTH_EAST_POSTCODES = array (
"DH-Durham",
"NE-Newcastle",
"DL-Darlington",-
"SR-Sunderland",
"HG-Harrogate",
"TS-Cleveland",
"HU-Hull",
"WF-Wakefield",
"LS-Leeds",
"YO-York"
);
echo("<b>Open NORTH_EAST_POSTCODES</b>" . "<br><ul>");
$day = date('D');
foreach($NORTH_EAST_MAP[$day] as $d) {
    printf("<li>".$NORTH_EAST_POSTCODES[$d] . "</li>");
}
echo "</ul>";



$NORTH_WEST_MAP = array(
"Mon" => array(0,6),
"Tue" => array(1,8),
"Wed" => array(3,7),
"Thu" => array(2,9),
"Fri" => array(4,5),
"Sat" => array(0,8),
"Sun" => array(6,9),
);
$NORTH_WEST_POSTCODES = array(
"BB-Blackburn",
"L-Liverpool",
"BD-Bradford",
"LA-Lancaster",
"BL-Bolton",
"M-Manchester",
"CA-Carlisle",
"OL-Oldham",
"CH-Chester",
"PR-Preston",
"CW-Crewe",
"SK-Stockport",
"fy-Fleetwood",
"WA-Warrington",
"HD-Huddersfield",
"WN-Wigan",
"HX-Halifax"
);
echo("<b>Open NORTH_WEST_POSTCODES</b>" . "<br><ul>");
$day = date('D');
foreach($NORTH_WEST_MAP[$day] as $d) {
    printf("<li>".$NORTH_WEST_POSTCODES[$d] . "</li>");
}
echo "</ul>";



$EAST_MIDLANDS_MAP = array(
"Mon" => array(0,6),
"Tue" => array(1,8),
"Wed" => array(3,7),
"Thu" => array(2,9),
"Fri" => array(4,5),
"Sat" => array(1,8),
"Sun" => array(6,9),
);
$EAST_MIDLANDS_POSTCODES = array(
"CB-Cambridge",
"LN-Lincoln",
"CO-Colchester",
"NG-Nottingham",
"DE-Derby",
"NR-Norwich",
"DN-Doncaster",
"PE-Peterborough",
"IP-Ipswich",
"S-Sheffield",
"LE-Leicester",
"SS-Southend"
);
echo("<b>Open EAST_MIDLANDS_POSTCODES</b>" . "<br><ul>");
$day = date('D');
foreach($EAST_MIDLANDS_MAP[$day] as $d) {
    printf("<li>".$EAST_MIDLANDS_POSTCODES[$d] . "</li>");
}
echo "</ul>";




$WEST_MIDLANDS__MAP = array(
"Mon" => array(0,6),
"Tue" => array(1,8),
"Wed" => array(3,7),
"Thu" => array(2,8),
"Fri" => array(4,5),
"Sat" => array(1,8),
"Sun" => array(6,8),
);
$WEST_MIDLANDS_POSTCODES = array(
"B-Birmingham",
"ST-Stoke on Trent",
"CV-Coventry",
"TF-Telford",
"DY-Dudley",
"WR-Worcester",
"HR-Hereford",
"WS-Walsall",
"NN-Northampton"
);
echo("<b>Open WEST_MIDLANDS_POSTCODES</b>" . "<br><ul>");
$day = date('D');
foreach($WEST_MIDLANDS__MAP[$day] as $d) {
    printf("<li>".$WEST_MIDLANDS_POSTCODES[$d] . "</li>");
}
echo "</ul>";




$WALES_POSTCODES_MAP = array(
"Mon" => array(0,3),
"Tue" => array(1,5),
"Wed" => array(3,2),
"Thu" => array(2,5),
"Fri" => array(4,5),
"Sat" => array(1,4),
"Sun" => array(1,5),
);
$WALES_POSTCODES = array(
"CF-Cardiff",
"NP-Newport",
"LD-Llandrindod",
"SA-SSwansea",
"LL-Llandudno",
"SY-Shrewsbury"
);
echo("<b>Open WALES_POSTCODES</b>" . "<br><ul>");
$day = date('D');
foreach($WALES_POSTCODES_MAP[$day] as $d) {
    printf("<li>".$WALES_POSTCODES[$d] . "</li>");
}
echo "</ul>";


$SOUTH_WEST_POSTCODES = array(
"BA-Bath",
"PL-Plymouth",
"SN-Swindon",
"BS-Bristol",
"SP-Salisbury",
"DT-Dorchester",
"TA-Taunton",
"EX-Exeter",
"TQ-Torquay",
"TR-Truro",
"GL-Gloucester"
);



$SOUTH_EAST_POSTCODES = array(
"AL-St. Albans",
"OX-Oxford",
"BN-Brighton",
"PO-Portsmouth",
"CM-Chelmsford",
"RG-Reading",
"CT-Canterbury",
"RH-Redhill",
"GU-Guilford",
"SG-Stevenage",
"HP-Hemel",
"SL-Slough",
"LU-Luton",
"SO-Southampton",
"ME-Medway",
"SS-Southend ",
"MK-Milton Keynes"
);


$GREATER_LONDON_POSTCODES = array(
"BR-Bromley",
"NW-London",
"CR-Croydon",
"RM-Romford",
"DA-Dartford",
"SE-London",
"SM-Sutton",
"EC-London",
"SW-London",
"EN-Enfield",
"TW-Twickenham",
"HA-Harrow",
"UB-Southall",
"IG-Ilford",
"W-London",
"KT-Kingston",
"WC-London",
"London N",
"WD-Watford"
);


?>





  </body>
</html>