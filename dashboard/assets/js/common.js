/*! common.js | Bulkit | CSS Ninja */

/* ==========================================================================
js API common file
========================================================================== */
window.google_sheet_signin = 0;
$(document).ready(function(){

    //Reusable functions in the app global scope
    (function ($) {

        $.fn.getCookie = function(name) {
            var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
            if (match) return match[2];
        };

        $.fn.removeCookie = function(name) {
            var d = new Date();
            d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = name + "=;" + expires + ";path=/";
        };

        $.fn.workOutTimeFromBalance = function(balance, rate, setRateTime) {
            if(setRateTime){
                var rateTimeInSeconds = setRateTime
            } else {
                if (!balance || !rate) return "00h 00m";
                var rateTimeInSeconds = Math.round((parseInt(balance) / (parseInt(rate)) * 24 * 3600));
            }

            var sec_num = parseInt(rateTimeInSeconds, 10); // don't forget the second param

            var hours   = Math.floor(sec_num / 3600);
            var days   = hours / 24;
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            // var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {minutes = "0"+minutes;}

            if(days > 1 ) {
                hours = hours - (Math.floor(days) * 24);

                if(!hours){
                    return Math.floor(days)+' days';
                }

                return Math.floor(days)+'d '+hours+'h';
            }
            return hours+'h '+minutes+'m';
        };

        $.fn.refreshAuth = function(){
            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/landing-v4-login');
                return
            }
            var data = {
                token: token,
            };
            $.ajax({
                url: 'https://jira.lovetheidea.co/public/_/auth/refresh?&activity_skip=1',
                method: 'POST',
                data: data,
                headers: {
                    "Authorization": "Bearer " + token
                }
            }).done(function (res) {
                var d = new Date();
                d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));
                var expires = "expires="+d.toUTCString();
                document.cookie = "au=" + res.data.token + ";" + expires + ";path=/";
            }).fail(function (e) {
                console.log(e)
            })
        };

        // get user details
        $.fn.getUserDetails = function(){
            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/');
                return
            }
            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: 'https://jira.lovetheidea.co/public/_/users/me',
                    type: 'GET',
                    data: 'offset=0&fields=*.*',
                    headers: {
                        "Authorization": "Bearer " + token
                    }
                }).done(function (res) {
                    resolve(res)
                }).fail(function (e) {
                    if (e.status == 403 || e.status == 401 || (e.status == 404)) {
                        window.location.replace('/landing-v4-login#timeout=' + (e.responseJSON ? e.responseJSON.error.code : '60'));
                    }
                    reject(e)
                })
            });

        };

        //get server token
        $.fn.serverInfo = function(){

        };


        // get companies
        $.fn.getCompanies = function(user_id){

            var id = user_id;
            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/landing-v4-login');
                return
            }

            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: 'https://jira.lovetheidea.co/public/_/items/company',
                    type: 'GET',
                    data: 'filters[created_by][eq]=' + id + '&filters[owner_2][logical]=or&filters[owner_2]=' + id + '&filters[owner_3][logical]=or&filters[owner_3]=' + id + "&fields=*,members_count.user.avatar.data,members_count.status,project_count.name,task_count.id,logo.data",
                    headers: {
                        "Authorization": "Bearer " + token
                    }
                }).done(function (res) {
                    resolve(res)
                }).fail(function (e) {
                    if (e.status == 403 || e.status == 401) {
                        window.location.replace('/lovetheidea.bitbucket.io');
                    }
                    reject(e);
                })
            });
        };


        $.fn.getProjects = function(user_id, company_id){

            var id = user_id;
            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/landing-v4-login');
                return
            }
            if(!company_id) {
                company_id = "gofish";
                return;
            }



            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: 'https://jira.lovetheidea.co/public/_/items/projects',
                    type: 'GET',
                    data: "filter[owner]=" + id + "&filter[company]=" + company_id + "&fields=*,total_t_count.summary,team.name",
                    headers: {
                        "Authorization": "Bearer " + token
                    }
                }).done(function (res) {

                    var projectsList = $('#tab-a1 .projects-list-wrapper .support-tickets tbody');
                    var projects = projectsList.children('tr');
                    if(!projects.length){
                        return
                    }
                    var project = projects.eq(0).clone(true);
                    projects.remove();
                    res.data.forEach(function (v) {
                        if (v.status !== 'published') return;
                        var p = project.clone(true);
                        p.find('.ticket-name span').text(v.name + ' (' + (v.total_t_count.length > 0 ? v.total_t_count.length : "1") + ')');
                        p.find('.ticket-id span').text($().addZero(v.id));
                        p.find('.team-list').text($().addZero(v.active));
                        p.find('a.ticket-name').attr('href', p.find('a.ticket-name').attr('href') + '/' + company_id + '/' + v.id)
                        projectsList.append(p);
                    });

                    resolve(res)

                }).fail(function (e) {
                    if (e.status == 403 || e.status == 401) {
                        window.location.replace('/lovetheidea.bitbucket.io');
                    }
                    reject(e);
                })
            })
        };

        $.fn.getTeams = function(company_id){

            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/landing-v4-login');
                return
            }
            if(!company_id) {
                company_id = "gofish";
                return;
            }



            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: 'https://jira.lovetheidea.co/public/_/items/teams',
                    type: 'GET',
                    data: "filter[company]=" + company_id + "&fields=*,members.user.avatar.data,projects.id&sort=name",
                    headers: {
                        "Authorization": "Bearer " + token
                    }
                }).done(function (res) {

                    var teamList = $('#tab-a2 .projects-list-wrapper .list-body .columns');
                    var teamEmpty = teamList.children('.column.is-4.team-empty');
                    var teamFull = teamList.children('.column.is-4.team-full');

                    var teamE = teamEmpty.eq(0).clone(true);
                    var teamF = teamFull.eq(0).clone(true);
                    teamEmpty.remove();
                    teamFull.remove();
                    res.data.forEach(function (v) {
                        if (v.status !== 'published') return;
                        var p = "";
                        if(!v.members.length){
                            p = teamE.clone(true);
                        } else {
                            p = teamF.clone(true);
                        }

                        var $team = v.name.replace(" Team", "");
                        p.find('.header h4').html("<a href='/dashboard/dashboard-crm-deal/"+ company_id +"/"+ $team.replace(" ", "_") +"'>" + $team + " Team </a>");
                        p.find('.team-info').text(v.projects.length + " Workflows, " + v.members.length + " Members");

                        if (v.members.length) {

                            var imageRow = p.find('.project-members');
                            var imageFaces = imageRow.find('.face:not(.add-button):not(.is-fake)');
                            var imageFace = imageFaces.eq(0).clone(true);

                            imageFaces.remove();

                            if(v.members.length > 5){
                                imageRow.find('.face.is-fake').text('+' + (v.members_count.length - 5));
                            } else {
                                imageRow.find('.face.is-fake').remove()
                            }

                            v.members.forEach(function(vv) {
                                var imgF = imageFace.clone(true);
                                if(vv.user.avatar && vv.user.avatar.data.thumbnails.length)
                                    imgF.find('img').attr("src", vv.user.avatar.data.thumbnails[2].url);
                                else
                                    imgF.find('img').attr("src" , "/assets/images/avatars/blank.png");

                                imageRow.prepend(imgF);
                                imageRow.removeClass('is-hidden');
                            })
                        }

                        teamList.append(p);
                    });

                    $('.ng-team-count').text(res.data.length);

                    resolve(res)

                }).fail(function (e) {
                    if (e.status == 403 || e.status == 401) {
                        window.location.replace('/lovetheidea.bitbucket.io');
                    }
                    reject(e);
                })
            })
        };

        $.fn.getMembers = function(company_id){

            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/landing-v4-login');
                return
            }
            if(!company_id) {
                company_id = "gofish";
                return;
            }



            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: 'https://jira.lovetheidea.co/public/_/items/members',
                    type: 'GET',
                    data: "filter[company]=" + company_id + "&fields=*,team.name,user.first_name,user.last_name,user.avatar.data&sort=id",
                    headers: {
                        "Authorization": "Bearer " + token
                    }
                }).done(function (res) {

                    var memberList = $('#tab-a3 .projects-list-wrapper .list-body .columns');
                    var member = memberList.children('.column.is-4');

                    var memberClone = member.eq(0).clone(true);
                    member.remove();

                    res.data.forEach(function (v) {
                        if (v.status !== 'published') return;
                        var p = memberClone.clone(true)

                        p.find('.name').text(v.user.first_name + " " + v.user.last_name);
                        if (v.user.avatar && v.user.avatar.data.thumbnails.length)
                            p.find('.member-image').attr("src" , v.user.avatar.data.thumbnails[2].url);
                        else
                            p.find('.member-image').attr("src" , "/assets/images/avatars/blank.png");
                        p.find('    .role').text(v.team.name);

                        memberList.append(p);
                    });

                    resolve(res)

                }).fail(function (e) {
                    if (e.status == 403 || e.status == 401) {
                        window.location.replace('/lovetheidea.bitbucket.io');
                    }
                    reject(e);
                })
            })
        };

        $.fn.getTasks = function(id, project_id){
            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/landing-v4-login');
                return
            }

            var url = 'https://jira.lovetheidea.co/public/_/items/projects';
            var data = "filter[owner]=" + id;

            if(project_id){
                data = data + "&filter[id]=" + project_id //+ "&fields=*,project.name,project.id,project.google_sheet_id,project.range";
            }

            var ticketsList = $('.tickets-table tbody');
            var tickets = ticketsList.children('tr');
            var ticket = tickets.eq(0).clone(true);
            tickets.remove();

            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    headers: {
                        "Authorization": "Bearer " + token
                    }
                }).done(function (res) {

                    // tickets.remove();
                    var closed_tickets = 0

                    res.data.forEach(function (v) {

                        $('.support-title').text('All Tasks > ' + v.name);
                        $('.ng-task-title').text(v.name);

                        //used in googlepreadsheet.js
                        window.google_sheet_id = v.google_sheet_id;
                        window.google_sheet_range = v.range;

                        window.google_sheet_ticket = ticket
                        window.google_sheet_ticketList = ticketsList
                        window.google_sheet_add = function(v, i) {

                            var ticketType = {
                                critical: '<i class="fas fa-bug bug"></i>',
                                bug: '<i class="fas fa-bug bug"></i>',
                                task: '<i class="fas fa-pen-square task-list"></i>',
                                low: '<i class="fas fa-pen-square task-list"></i>',
                                high: '<i class="fas fa-pen-square issue-list"></i>',
                                BUG: '<i class="fas fa-bug bug"></i>',
                                CRITICAL: '<i class="fas fa-bug bug"></i>',
                                TASK: '<i class="fas fa-pen-square task-list"></i>',
                                LOW: '<i class="fas fa-pen-square task-list"></i>',
                                HIGH: '<i class="fas fa-pen-square issue-list"></i>',
                            };
                            var status = {
                                open: ['is-open', 'Pending'],
                                low: ['is-in-progress', 'Low'],
                                high: ['is-escalated', 'Escalated'],
                                new: ['is-new', 'New'],
                                closed: ['is-closed', 'Closed'],
                                OPEN: ['is-open', 'Pending'],
                                LOW: ['is-in-progress', 'Low'],
                                HIGH: ['is-escalated', 'High'],
                                NEW: ['is-new', 'New'],
                                CLOSED: ['is-closed', 'Closed'],

                            };

                            var ticket = window.google_sheet_ticket;
                            var p = ticket.clone(true);

                            p.find('.ticket-id').html((ticketType[v[1]] ? ticketType[v[1]] : ticketType['task']) +  (v[5]? v[5] : "G-" + i));
                            p.find('.ticket-name span').text(v[2]);
                            p.find('.modifications .date').html((v[3] && v[3].length <= 7) ? v[3] : "--");
                            p.find('.ticket-age').html("<span>" + (v[8] && v[8].length <= 7) ? v[8] : "--" + "</span>");

                            if (v[1] === '' && status[v[1]]) {
                                p.attr('class', status[v[1]][0]);
                                p.find('td.status').attr('class', 'status');
                            } else {
                                p.find('td.status').attr('class', 'status ' + status['open'][0]);
                            }

                            p.find('td.status span.tag').text(status[v[1]] ? status[v[1]][1] : status['open'][1]);
                            p.find('td.priority-type').attr('class', 'priority-type is-' + v[1]);
                            p.find('img').attr('src', 'assets/images/avatars/avatar.png');
                            window.google_sheet_ticketList.append(p);

                            // if (v.workflow != 'open') {
                            //     closed_tickets++
                            // }
                        }
                        if(window.google_sheet_signin) {
                            updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                        }
                    });

                    $('.tickets-count').text(res.data.length + " tickets");
                    $('.tickets-closed').text(closed_tickets + " closed");

                    resolve(res)

                }).fail(function (e) {
                    if (e.status == 403 || e.status == 401) {
                        window.location.replace('/lovetheidea.bitbucket.io');
                    }
                    reject(e);
                })
            })
        };


        $.fn.getUserActivities = function(user_id){

            var id = user_id
            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/');
                return
            }

            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: 'https://jira.lovetheidea.co/public/_/activity',
                    type: 'GET',
                    data: 'filter[action_by]=' + id + "&sort=-action_on&activity_skip=1&limit=100",
                    headers: {
                        "Authorization": "Bearer " + token
                    }
                }).done(function (res) {
                    var data = res.data;
                    if (res.data) {
                        $().populateUserActivities(data);
                    }
                    resolve(res)
                }).fail(function (e) {
                    if (e.status == 403 || e.status == 401) {
                        window.location.replace('/lovetheidea.bitbucket.io');
                    }
                    reject(e)
                })
            })
        };

        $.fn.populateCompanyDetails = function(json_res) {

            var res = json_res;
            var container = $('.boards-grid .columns.is-multiline');
            var items = $('.boards-grid .columns.is-multiline>.column.is-4');
            var item =items.eq(0).clone(true);
            if(res.data.length){
                items.remove();
                var company_funds = 0;
                var project_count = 0;
                var task_count = 0;
                var member_count = 0;
                var total_balance_rate_seconds = 0;

                res.data.forEach(function(v){
                    if(v.status != "published") return;

                    total_balance_rate_seconds = total_balance_rate_seconds + Math.round((parseInt(v.balance) / (parseInt(v.rate)) * 24 * 3600))

                    var i = item.clone(true);
                    i.find('.ng-edit-project-link').attr('href', i.find('.ng-edit-project-link').attr('href') + '/' + v.id)
                    i.find('.ng-edit-company-link').attr('href', i.find('.ng-edit-company-link').attr('href') + '/' + v.id)
                    i.find('.add-company-id').attr('href', 'dashboard-project-list/' + v.id)

                    i.find('.card-inner .project-total').text(v.project_count ? v.project_count.length : 0);
                    i.find('.card-inner .task-total').text(v.task_count ? v.task_count.length : 0);
                    i.find('.card-inner .members-total').text(v.members_count ? v.members_count.filter(function(w){ return w.status == "published"}).length : 0);
                    i.find('.card-inner h3.board-title').text(v.title);
                    if(v.logo)
                        i.find('.card-inner .board-icon img').attr('src' , v.logo.data.thumbnails[5].url);

                    if (v.members_count.length) {

                        var imageRow = i.find('.board-assignees');
                        var imageFaces = imageRow.find('.assignee');
                        var imageFace = imageFaces.eq(0).clone(true);

                        imageFaces.remove();

                        if(v.members_count.length > 5){
                            imageRow.find('.is-more').text('+' + (v.members_count.length - 5));
                        } else {
                            imageRow.find('.is-more').remove()
                        }

                        v.members_count.forEach(function(vv) {
                            var imgF = imageFace.clone(true);
                            if(vv.user.avatar && vv.user.avatar.data.thumbnails.length)
                                imgF.attr("src", vv.user.avatar.data.thumbnails[2].url);
                            else
                                imgF.attr("src" , "/assets/images/avatars/blank.png");

                            imageRow.prepend(imgF);
                            imageRow.removeClass('is-hidden');
                        })
                    }

                    i.find('.card-foot .footer-block-clear span.block-time').text($().workOutTimeFromBalance(parseInt(v.balance), parseInt(v.rate)));
                    i.find('.card-foot .footer-block span.block-balance').text(v.balance.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}));
                    i.show();

                    if(!v.active){
                        i.addClass('warning')
                    }
                    if(!parseInt(v.balance)){
                        i.addClass('nomoney')
                    }
                    container.append(i);

                    company_funds = company_funds + (parseInt(v.balance));
                    project_count = project_count + (v.project_count ? v.project_count.length : 0);
                    task_count = task_count + (v.task_count ? v.task_count.length : 0);
                    //member_count = member_count + (v.members_count ? v.member_count.length : 0);
                    member_count = member_count + (v.members_count ? v.members_count.filter(function(w){ return w.status == "published"}).length : 0);
                });

                $('.ng-time-balance-only-total').text($().workOutTimeFromBalance(null, null, total_balance_rate_seconds));

                $('.total_fund').text(company_funds.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}));
                $('#profile-cover .followers .stat-val').text(project_count);
                $('#profile-cover .following .stat-val').text(task_count);
                $('#profile-cover .likes .stat-val').text(member_count);
            }
        };

        $.fn.getCompanyById = function(data, id) {
            return data.filter(
                function(data){ return data.id == id }
            );
        }

        $.fn.getCompanyServiceByRate = function(rate) {
            var rate = parseInt(rate);
            return rate + " GBP";

            if(rate < 500){
                return "Economy"
            }
            if(rate > 500 && rate < 1000){
                return "Plus"
            }
            if(rate > 1000 && rate < 5000){
                return "Exec"
            }
        }

        $.fn.populateCompanyDetailsWithId = function(json_res, company_id) {

            function hashInt (str) {
                var hash = 0;
                for (var i = 0; i < str.length; i++) {
                    hash = ((hash << 5) - hash) + str.charCodeAt(i);
                    hash = hash & hash;
                }
                return Math.round(2352422 * Math.abs(hash) / 1);
            }

            var res = json_res;
            if(res.data.length){
                var project_count = 0;
                var task_count = 0;
                var member_count = 0;

                var v = $().getCompanyById(res.data, company_id);
                v = v && v.length ? v[0] : false;

                if(!v){
                    return;
                }

                project_count = project_count + (v.project_count ? v.project_count.length : 0);
                task_count = task_count + (v.task_count ? v.task_count.length : 0);
                //member_count = member_count + (v.members_count ? v.members_count.length : 0);
                member_count = member_count + (v.members_count ? v.members_count.filter(function(w){ return w.status == "published"}).length : 0);

                $('a.support-title-a').attr('href', $('a.support-title-a').attr('href') + '/' + company_id)

                $('.ng-project-count').text(project_count);
                $('.ng-project-tagline').text(v.tagline ? v.tagline : "...");
                $('.ng-member-count').text(member_count);
                $('.ng-time-balance-only').text($().workOutTimeFromBalance(parseInt(v.balance), parseInt(v.rate)));
                $('.ng-time-balance').text($().workOutTimeFromBalance(parseInt(v.balance), parseInt(v.rate)) + " | + £" + v.balance.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}));
                $('.ng-balance').text("+ £" + v.balance.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}));
                $('.ng-project-title').text(v.title);
                if(v.logo)
                    $('.ng-project-icon').attr("src", v.logo.data.thumbnails[5].url);
                $('.ng-project-sid').html("<i class=\"material-icons\">link</i><a style='\n" +
                    "    height: 24px;\n" +
                    "    display: inline-block;\n" +
                    "    vertical-align: text-bottom;\n" +
                    "    margin-left: 5px;\n' href='" + v.jira + "'>JIRA</a>");
                $('.ng-rate').text($().getCompanyServiceByRate(v.rate));


                if (v.members_count.length) {

                    var imageRow = $('.project-members-avatars');
                    var imageFaces = imageRow.find('.face:not(.add-button):not(.is-fake)');
                    var imageFace = imageFaces.eq(0).clone(true);

                    imageFaces.remove();

                    if(v.members_count.length > 5){
                        imageRow.find('.face.is-fake').text('+' + (v.members_count.length - 5));
                    } else {
                        imageRow.find('.face.is-fake').remove()
                    }

                    v.members_count.forEach(function(vv) {
                        var imgF = imageFace.clone(true);
                        if(vv.user.avatar && vv.user.avatar.data.thumbnails.length)
                            imgF.find('img').attr("src", vv.user.avatar.data.thumbnails[2].url);
                        else
                            imgF.find('img').attr("src" , "/assets/images/avatars/blank.png");

                        imageRow.prepend(imgF);
                        imageRow.removeClass('is-hidden');
                    })
                }

            }
        }

        $.fn.populateUserDetails = function(data){
            console.log(data)
            var avatar = data.avatar ? data.avatar.data.thumbnails[0].url : "/assets/images/avatars/blank.png";
            $('.user-info-avatar').attr('src', avatar);
            $('.user-info-avatar').attr('data-demo-src',avatar);
            $('.user-info-name').text(data.first_name + ' ' + data.last_name);
            $('.first-name-input').val(data.first_name);
            $('.last-name-input').val(data.last_name);
            $('.user-info-email').text(data.email);
            $('.email-input').val(data.email);
            $('.user-info-company').text(data.company);
            $('.company-input').val(data.company);
            $('.position-input').val(data.company);
            $('.user-info-position').text(data.title);

            $('#profile-cover .follow').text('Status: ' + data.status);
            $('#profile-cover .tagline').text('Last seen: ' + $().time_ago(data.last_access_on));
        };

        $.fn.populateUserActivities = function(data){
            var container = $('.user-list');
            var items = $('.user-list > li');
            var item =items.eq(0).clone(true);
            if(data) {
                items.remove();
                var counter = 0;
                for (var count = 0; count < (data.length) ; count++) {
                    if(counter > 100){
                        break;
                    }
                    counter++;
                    var i = item.clone(true);
                    var v  = data[count];
                    var type = v.collection.replace("directus_", "");
                    i.find('.user-status .name').text(v.action);
                    if(v.action == 'invalid-credentials'){
                        i.find('.user-list-avatar').attr("src", "assets/images/avatars/action6.png");
                    }
                    if(v.action == 'authenticate'){
                        i.find('.user-list-avatar').attr("src", "assets/images/avatars/action5.png");
                    }
                    if(v.action == 'soft-delete'){
                        i.find('.user-list-avatar').attr("src", "assets/images/avatars/action7.png");
                    }
                    if(type == 'company'){
                        i.find('.user-list-avatar').attr("src", "assets/images/avatars/action3.png");
                    }
                    if(type == 'permissions' || type == 'user_roles'){
                        i.find('.user-list-avatar').attr("src", "assets/images/avatars/action2.png");
                    }
                    if(type == 'fields'){
                        i.find('.user-list-avatar').attr("src", "assets/images/avatars/action1.png");
                    }

                    i.find('.user-status .role').text($().time_ago(v.action_on));
                    i.find('.user-status .status').text(type);
                    i.show();
                    container.append(i);
                }

            }
            // if(data.avatar) {
            //     $('.user-info-avatar').attr('src', (data.avatar.data.thumbnails[0].url));
            //     $('.user-info-avatar').attr('data-demo-src', data.avatar.data.thumbnails[0].url);
            // }
            // $('.user-info-name').text(data.first_name + ' ' + data.last_name);
            // $('.user-info-email').text(data.email);
            // $('.user-info-company').text(data.company);
            // $('.user-info-position').text(data.title);
        }

        $.fn.time_ago = function(time) {

            switch (typeof time) {
                case 'number':
                    break;
                case 'string':
                    time = +new Date(time);
                    break;
                case 'object':
                    if (time.constructor === Date) time = time.getTime();
                    break;
                default:
                    time = +new Date();
            }
            var time_formats = [
                [60, 'seconds', 1], // 60
                [120, '1 minute ago', '1 minute from now'], // 60*2
                [3600, 'minutes', 60], // 60*60, 60
                [7200, '1 hour ago', '1 hour from now'], // 60*60*2
                [86400, 'hours', 3600], // 60*60*24, 60*60
                [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
                [604800, 'days', 86400], // 60*60*24*7, 60*60*24
                [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
                [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
                [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
                [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
                [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
                [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
                [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
                [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
            ];
            var seconds = (+new Date() - time) / 1000,
                token = 'ago',
                list_choice = 1;

            if (seconds == 0) {
                return 'Just now'
            }
            if (seconds < 0) {
                seconds = Math.abs(seconds);
                token = 'from now';
                list_choice = 2;
            }
            var i = 0,
                format;
            while (format = time_formats[i++])
                if (seconds < format[0]) {
                    if (typeof format[2] == 'string')
                        return format[list_choice];
                    else
                        return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
                }
            return time;
        };

        $.fn.addZero = function(n){
            n = String(n);
            return (n.length >= 4) ? n : new Array(5 - n.length).join('0') + n;
        };

        $.fn.addProject = function(data) {
            var token = $().getCookie("au");

            if(!token){
                window.location.replace('/landing-v4-login');
                return
            }

            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: 'http://jira.lovetheidea.co/public/_/items/projects',
                    type: 'POST',
                    headers: {
                        "Authorization": "Bearer " + token
                    },
                    data: data
                }).done(function (res) {
                    window.location.replace('/dashboard/dashboard-chat-app');
                    resolve(res)

                }).fail(function (e) {
                    Swal.fire({
                        imageUrl : '/assets/images/illustrations/drawings/undraw_cancel_u1it.svg',
                        title: 'Oops...',
                        text: 'Something went wrong, please, try again.',

                    });
                    reject(e);
                    console.log(e)
                })
            })
        };

        $.fn.addPayPal = function(){
            paypal.Buttons({
                createOrder: function(data, actions) {
                    console.log(data)
                    console.log(actions)
                    return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: '0.01'
                            }
                        }]
                    });
                },
                onApprove: function(data, actions) {
                    return actions.order.capture().then(function(details) {
                        alert('Transaction completed by ' + details.payer.name.given_name);
                        // Call your server to save the transaction
                        return fetch('/paypal-transaction-complete', {
                            method: 'post',
                            headers: {
                                'content-type': 'application/json'
                            },
                            body: JSON.stringify({
                                orderID: data.orderID
                            })
                        });
                    });
                }
            }).render('#paypal-button-container');
        }

    }(jQuery));

})


/* ==========================================================================
js pageloader file
========================================================================== */


$(document).ready(function($) {

    "use strict";

    //Page loader
    if ($('.pageloader').length) {

        $('.pageloader').toggleClass('is-active');

        $(window).on('load', function () {
            var pageloaderTimeout = setTimeout(function () {
                $('.pageloader').toggleClass('is-active');
                $('.infraloader').toggleClass('is-active')
                clearTimeout(pageloaderTimeout);
            }, 700);
        })
    }

    $('#materialInputSearch').on('change', function () {
        $('#search-modal .modal-background').toggleClass('scaleInCircle');
        $('#search-modal .modal-content').toggleClass('scaleIn');
        $('#search-modal .modal-close').toggleClass('is-hidden');
        //Restore native body scroll
        if ($('.dashboard-wrapper').length) {
            $('body').removeClass('is-fixed');
        }
        setTimeout(function () {
            $('.modal.is-active').removeClass('is-active');
            //Restore sticky nav and backktotop
            $('#scrollnav, #backtotop').toggleClass('is-hidden');

        }, 500);
        var titles = $('.boards-grid .board-title');
        if (titles && !titles.length) {
            titles = $('.project-card h4 a');
        }
        var val = $(this).val().trim();
        $('input[data-modal=search-modal]').val(val);
        if (val === '') {
            titles.each(function (i, v) {
                $(v).closest('.column').css('display', 'block');
            })
        } else {
            var bool = false;
            titles.each(function (i, v) {
                var column = $(v).closest('.column');
                if ($(v).text().search(new RegExp(val, 'i')) > -1) {
                    column.css('display', 'block');
                    bool = true;
                } else {
                    column.css('display', 'none');
                }
            });

            $('.no-search-result-found').css('display', (bool ? 'none' : 'block'));
        }
    });

    // quickview-trigger paused

    $('.quickview-trigger').on('click', function (e) {
        var title = $(e.target).closest('.column').find('h3.board-title').text().trim();
        $('.chatIndustryName').text(title);

        var name = $(".navbar-menu.chat-nav .dropdown.is-spaced .dropdown-item h5.is-narrow").text().trim();
        $('.chatProfileName').text(name);
    });

    // edit company name

    $('.edit-company-name').on('click', function (e) {
        var el = $(".prop-group .field__text, h2.project-title");
        var tag = $(".project-tagline")

        Swal.fire({
            title: 'Change company name',
            html:
                '<input id="swal-input1-company" class="swal2-input" placeholder="Company Name" value="' + el.eq(0).text() + '">' +
                '<input id="swal-input2-tag" class="swal2-input" placeholder="Tag Line" value="' + tag.text() + '">',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#swal-input1-company').val(),
                        $('#swal-input2-tag').val()
                    ]);
                })
            },
            showCancelButton: true,
            confirmButtonText: 'Confirm',
            showLoaderOnConfirm: true
        })
            .then(function (result) {
                if (result.value.length) {
                    if (!result.value[0].trim()) return;
                    el.text(result.value[0].trim());
                    tag.text(result.value[1].trim());
                }
            })
    });

    $('.edit-project-name').on('click', function (e) {
        var el = $(e.target).closest('.header').find('h4>a').eq(0);

        Swal.fire({
            title: 'Change project name',
            input: 'text',
            inputValue: el.text(),
            showCancelButton: true,
            confirmButtonText: 'Confirm',
            showLoaderOnConfirm: true
        })
            .then(function (result) {
                if (result.value) {
                    if (!result.value.trim()) return;
                    el.text(result.value);
                }
            })
    });

    $('#searchForTask').on('change', function () {
        $('#search-modal .modal-background').toggleClass('scaleInCircle');
        $('#search-modal .modal-content').toggleClass('scaleIn');
        $('#search-modal .modal-close').toggleClass('is-hidden');
        //Restore native body scroll
        if ($('.dashboard-wrapper').length) {
            $('body').removeClass('is-fixed');
        }
        setTimeout(function () {
            $('.modal.is-active').removeClass('is-active');
            //Restore sticky nav and backktotop
            $('#scrollnav, #backtotop').toggleClass('is-hidden');

        }, 500);
        var task = $('tbody .ticket-name');
        if (task && !task.length) {
            task = $('.project-card h4 a');
        }
        var val = $(this).val().trim();
        $('input[data-modal=search-modal]').val(val);
        if (val === '') {
            task.each(function (i, v) {
                $(v).closest('tr').css('display', 'table-row');
            });
            $('.no-search-result-found').css('display', ('none'));
        } else {
            var bool = false;
            task.each(function (i, v) {
                var column = $(v).closest('tr');
                if ($(v).text().search(new RegExp(val, 'i')) > -1) {
                    column.css('display', 'table-row');
                    bool = true;
                } else {
                    column.css('display', 'none');
                }
            });
            $('.no-search-result-found').css('display', (bool ? 'none' : 'block'));
        }
    });

    $('#newCompanyForm .input, #newCompanyForm select').on('change', function () {
        var bool = false;
        $('#newCompanyForm .input, #newCompanyForm select').each(function (i, v) {
            if (!$(v).val().trim()) {
                bool = true;
            }
        });
        $('#newCompanyForm button').prop('disabled', bool);
    });

//    create team

    $('#create-team-modal input').on('input change', function (e) {
        var el = $(e.target);
        $('#create-team-button').prop('disabled', !Boolean(el.val().trim()));
    });

    $('#create-team-button').on('click', function (e) {
        var teamNameTag = $('#create-team-modal input');
        var teamName = teamNameTag.val().trim();
        if (!teamName) {
            e.stopImmediatePropagation();
            return;
        }
        var columns = $('#tab-a2 .list-body .columns');
        var column = columns.find('.column').eq(0).clone(true);
        column.find('.header h4').text(teamName);
        column.find('.team-info').text('0 Project, Waiting for members...');
        column.find('.project-members').html('<div class="face"></div>');
        columns.prepend(column);
        teamNameTag.val('');
        $(this).prop('disabled', true);
    });

    // send refer

    $('#invite-user-modal').on('click', '.add-more', function () {
        var field = $('#invite-user-modal .field').eq(0).clone(true);
        field.find('input').val('');
        $(this).before(field);
        field.find('input').focus();
    });

    $('#invite-user-modal').on('input change', 'input', function (e) {
        var el = $(e.target);
        $('#invite-user-modal button[type=submit]').prop('disabled', !Boolean(el.val().trim()));
    });

    $('#invite-user-modal').on('click', 'button[type=submit]', function () {
        var inps = $('#invite-user-modal input');
        var bool = false;
        for (var i = 0; i < inps.length; i++) {
            if (inps[i].value.trim()) {
                bool = true;
                break;
            }
        }
        if (bool) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You are going to send referral links via email to your network!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Send'
            }).then(function (result) {
                if (result.value) {
                    Swal.fire(
                        'Sent!',
                        'Your file has been sent.',
                        'success'
                    ).then(function () {
                        var inp = $('#invite-user-modal input').eq(0);
                        inp.val('');
                        inp.closest('.field').nextAll('.field').remove();
                        $('#invite-user-modal button[type=submit]').prop('disabled', true);
                        $('#invite-user-modal .modal-background').toggleClass('scaleInCircle');
                        $('#invite-user-modal .modal-content').toggleClass('scaleIn');
                        $('#invite-user-modal .modal-close').toggleClass('is-hidden');
                        //Restore native body scroll
                        if ($('.dashboard-wrapper').length) {
                            $('body').removeClass('is-fixed');
                        }
                        setTimeout(function () {
                            $('.modal.is-active').removeClass('is-active');
                            //Restore sticky nav and backktotop
                            $('#scrollnav, #backtotop').toggleClass('is-hidden');

                        }, 500);
                    })
                }
            })
        }
    })

    //add additional admin

    $('#add_additional_admin').on('click', function (e) {
        Swal.fire({
            title: '+ Add New Admin',
            html:
                '<input id="swal-input-admin-email" type="email" class="swal2-input" placeholder="*Enter email address" value="">',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#swal-input-admin-email').val()
                    ]);
                })
            },
            closeOnConfirm: false,
            showCancelButton: true,
            confirmButtonText: 'Add additional admin',
            showLoaderOnConfirm: true,
            imageUrl: '/assets/images/illustrations/drawings/undraw_hire_te5y.svg'
        })
            .then(function (result) {
                if (result.value.length && result.value[0]) {
                    Swal.fire({
                        imageUrl: '/assets/images/illustrations/drawings/undraw_celebrating_bol5.svg',
                        title: 'Sent!',
                        text: 'The admin has been successfully added.',
                    });

                } else {
                        Swal.fire({
                            imageUrl: '/assets/images/illustrations/drawings/undraw_cancel_u1it.svg',
                            title: 'Please, try again ...',
                            text: 'Please check the email you entered',
                        })
                }
            })
    });

    $('.trigger-logout').on('click', function (e) {
        var token = $().removeCookie("au");

        if(!token){
            window.location.replace('/');
            return
        }
    });

    //http://jira.lovetheidea.co/public/_/items/projects?filter[owner]=1&filter[status]=published&limit=1&meta=result_count
});