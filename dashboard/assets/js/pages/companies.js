/*! dashboard.js | Bulkit | CSS Ninja */

/* ==========================================================================
Dashboard core JS file
========================================================================== */

$(document).ready(function($){

    "use strict";

    $().getUserDetails().then( function(res){
        var data = window.details = res.data;
        if (res.data) {
            $().populateUserDetails(data);

            $().getCompanies(data.id).then( function(res){
                $().populateCompanyDetails(res)
            });

            $().getUserActivities(data.id);

            //reauth to keep use logged in
            $().refreshAuth();
        }
    })


})

