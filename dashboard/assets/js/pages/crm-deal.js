/*! dashboard.js | Bulkit | CSS Ninja */

/* ==========================================================================
Dashboard core JS file
========================================================================== */

$(document).ready(function($){

    "use strict";

    window.details = {};

    const segment = new URL(window.location.href).pathname.split('/');
    var team = segment.pop();
    if (team == ""){
        team = segment.pop();
    }
    var company_id = segment.pop();

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    $().getUserDetails().then( function(res){
        var data = window.details = res.data;
        if (res.data) {
            $().populateUserDetails(data);

            $().getUserActivities(data.id);

            $().getCompanies(data.id).then( function(res){
                $().populateCompanyDetailsWithId(res, company_id)
            });

            $().getProjects(data.id , company_id);

            //reauth to keep use logged in
            $().refreshAuth();
        }
    });

    $('.ng-project').attr('href', $('.ng-project').attr('href') + '/' + company_id)
    $('.ng-balance-link').attr('href', $('.ng-balance-link').attr('href') + '/' + company_id)
    $('.ng-team-title').text(capitalize(team.replace("_", " ")));

    $().addPayPal();

});

