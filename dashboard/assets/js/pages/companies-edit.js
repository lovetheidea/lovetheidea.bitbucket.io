/*! dashboard.js | Bulkit | CSS Ninja */

/* ==========================================================================
Dashboard core JS file
========================================================================== */

$(document).ready(function($){

    "use strict";

    window.details = {}

    const segment = new
    URL(window.location.href).pathname.split('/').filter(Boolean).pop();
    var company_id = segment;

    $().getUserDetails().then( function(res){
        var data = window.details = res.data;
        if (res.data) {
            $().populateUserDetails(data);

            if(isNaN(company_id)){
                window.location.href = "dashboard-crm-boards";
            }

            $().getCompanies(data.id).then( function(res){
                $().populateCompanyDetailsWithId(res, company_id)
            });

            $().getProjects(data.id , company_id);

            //reauth to keep use logged in
            $().refreshAuth();
        }
    })

    $('.ng-project').attr('href', $('.ng-project').attr('href') + '/' + company_id)
    $('.ng-balance-link').attr('href', $('.ng-balance-link').attr('href') + '/' + company_id)
    $('[data-modal="invite-user-modal"]').attr('data-ref', company_id)

})

