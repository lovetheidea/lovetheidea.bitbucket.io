/*! dashboard.js | Bulkit | CSS Ninja */

/* ==========================================================================
Dashboard core JS file
========================================================================== */

$(document).ready(function($){

    "use strict";

    window.details = {};

    const segment = new
    URL(window.location.href).pathname.split('/').filter(Boolean).pop();
    var company_id = segment;

    $().getUserDetails().then( function(res){
        var data = window.details = res.data;
        if (res.data) {
            $().populateUserDetails(data);

            if(isNaN(company_id)){
                window.location.href = "dashboard-crm-boards";
            }

            $().getCompanies(data.id).then( function(res){
                $().populateCompanyDetailsWithId(res, company_id)
            });

            $().getTeams(company_id);
            $().getMembers(company_id);

            $().getProjects(data.id , company_id);

            //reauth to keep use logged in
            $().refreshAuth();
        }
    });

    $('.ng-rate').attr('href', $('.ng-rate').attr('href') + '/' + company_id)
    $('#dashboard-menu .parent-link.projects-all').attr('href', $('#dashboard-menu .parent-link').attr('href') + '/' + company_id)
    $('.ng-balance-link').attr('href', $('.ng-balance-link').attr('href') + '/' + company_id)
    $('[data-modal="invite-user-modal"]').attr('data-ref', company_id)

    $('#addProjectForm').on('submit', function(e){
        e.preventDefault();
        var projectName = $('#addProjectForm input[name=name]'),
        projectDesciption = $('#addProjectForm textarea[name=description]'),
        nameValue = projectName.val().trim(),
        descriptionValue = projectDesciption.val().trim();
        if(!nameValue || !descriptionValue){
            Swal.fire({
                imageUrl : '/assets/images/illustrations/drawings/undraw_cancel_u1it.svg',
                title: 'Empty fields...',
                text: 'Please enter project name and description'
            });
            return;
        }

        let data = {
            "status": "published",
            "sort": null,
            "created_by": details.id,
            "owner": details.id,
            "name": nameValue,
            "description": descriptionValue,
            "active": 1,
            "company": company_id
        };

        $().addProject(data);
        $().refreshAuth();
    });

})

