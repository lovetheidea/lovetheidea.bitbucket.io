/*! dashboard.js | Bulkit | CSS Ninja */

/* ==========================================================================
Dashboard core JS file
========================================================================== */

$(document).ready(function($){

    "use strict";

    window.details = {};

    const segment = new URL(window.location.href).pathname.split('/');
    var project_id = segment.pop();
    if (project_id == ""){
        project_id = segment.pop();
    }
    var company_id = segment.pop();

    $().getUserDetails().then( function(res){
        var data = window.details = res.data;
        if (res.data) {
            $().populateUserDetails(data);

            if(isNaN(company_id)){
                window.location.href = "dashboard-crm-boards";
            }

            $().getCompanies(data.id).then( function(res){
                $().populateCompanyDetailsWithId(res, company_id)
            });

            $().getTasks(data.id, project_id);

            //reauth to keep use logged in
            $().refreshAuth();
        }
    });

})

